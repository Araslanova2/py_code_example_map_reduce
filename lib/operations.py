import heapq
import numpy as np
import string
import typing as tp


from abc import abstractmethod, ABC
from datetime import datetime, timedelta
from collections import Counter
from copy import deepcopy
from itertools import groupby
from math import radians, cos, sin, asin, sqrt


TRow = tp.Dict[str, tp.Any]
TRowsIterable = tp.Iterable[TRow]
TRowsGenerator = tp.Generator[TRow, None, None]


def default_generator() -> TRowsGenerator:
    row: tp.Dict[str, tp.Any] = {}
    yield row


class Operation(ABC):
    @abstractmethod
    def __call__(self, rows: TRowsIterable, *args: tp.Any, **kwargs: tp.Any) -> TRowsGenerator:
        pass


# Operations


class Mapper(ABC):
    """Base class for mappers"""
    @abstractmethod
    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: one table row
        """
        pass


class Map(Operation):
    """Operation, that Call Mapper generator of each row of a table"""
    def __init__(self, mapper: Mapper) -> None:
        """
        :param mapper: a function that get a row and yield rows
        """
        self.mapper = mapper

    def __call__(self, rows: TRowsIterable, *args: tp.Any, **kwargs: tp.Any) -> TRowsGenerator:
        """
        :param rows: table rows
        """
        for row in rows:
            for new_row in self.mapper(row):
                yield new_row


class Reducer(ABC):
    """Base class for reducers"""
    @abstractmethod
    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        pass


class Reduce(Operation):
    """Operation that groups rows by a keys' subset
    and apply reducer function to each row of a table during __call__.
    Rows should be sorted by group keys before the call
    """
    def __init__(self, reducer: Reducer, keys: tp.List[str]) -> None:
        """
        :param reducer: a function that get a bunch of rows
        with the same group key and yield rows

        :param keys: group key (subset of row's keys)
        """
        self.reducer = reducer
        self.keys = keys
        self._used_keys: tp.Set[tp.Any] = set()

    def _raise_exception_if_nonsorted(self, key: tp.Any) -> None:
        """
        :param key: key to be tested for use earlier
        """
        key_tuple = tuple(key)
        if key_tuple in self._used_keys:
            raise RuntimeError("Reduce unsorted data Error")
        self._used_keys.add(key_tuple)

    def __call__(self, rows: TRowsIterable, *args: tp.Any, **kwargs: tp.Any) -> TRowsGenerator:
        """
        :param rows: table rows
        """
        for group_key, rows_group in groupby(rows, lambda row: [row[key] for key in self.keys]):
            self._raise_exception_if_nonsorted(group_key)
            for new_row in self.reducer(self.keys, list(rows_group)):
                yield new_row
        self._used_keys = set()


class Joiner(ABC):
    """Base class for joiners"""
    def __init__(self, suffix_a: str = '_1', suffix_b: str = '_2') -> None:
        """
        :param suffix_a: will be append as a suffix to the key names
        in the first table in case of the keys coincidence in tables
        :param suffix_b: will be append as a suffix to the key names
        in the second table in case of the keys coincidence in tables
        """
        self._a_suffix = suffix_a
        self._b_suffix = suffix_b

    @abstractmethod
    def __call__(self, keys: tp.Sequence[str], rows_a: TRowsIterable, rows_b: TRowsIterable) -> TRowsGenerator:
        """
        :param keys: join keys
        :param rows_a: left table rows
        :param rows_b: right table rows
        """
        pass


class Join(Operation):
    """Operation that union information from 2 tables
    based on keys and a joiner-rule
    """
    def __init__(self, joiner: Joiner, keys: tp.Sequence[str]):
        """
        :param joiner: a function that get 2 groups of rows
        (with the same group keys if group keys != []) and yield rows
        """
        self.joiner = joiner
        self.keys = keys
        self._used_keys: tp.List[tp.Any] = list()

    def _raise_exception_if_nonsorted(self, key: tp.Any, do_not_check: bool = False,
                                      other_branch_key: tp.Any = None) -> None:
        """
        :param key: key to be tested for use earlier
        :param do_not_check: flag if the checking will not be done
        :param other_branch_key: if the key exists and equal
        the tested key, error will not raised
        """
        if not do_not_check:
            key_tuple = tuple(key)
            if key_tuple in self._used_keys:
                if other_branch_key is None or (other_branch_key is not None and key_tuple != tuple(other_branch_key)):
                    raise RuntimeError("Join unsorted data Error")
            else:
                self._used_keys.append(key_tuple)

    def __call__(self, rows: TRowsIterable, *args: tp.Any, **kwargs: tp.Any) -> TRowsGenerator:
        """
        :param rows: first table rows
        :param args: second table rows should be in args[0].
        the strange design because of the parent class constraints
        """

        if self.keys:
            a_tuple: tp.List[tp.Any] = list()
            iter_nothing: tp.Iterable[tp.Any] = iter([])
            default_next = (a_tuple, iter_nothing)

            left_groups = groupby(rows, lambda row: [row[key] for key in self.keys])
            right_groups = groupby(args[0], lambda row: [row[key] for key in self.keys])
            left_key, left_group = next(left_groups, default_next)
            right_key, right_group = next(right_groups, default_next)

            self._raise_exception_if_nonsorted(left_key, left_group == iter_nothing)
            if right_group != iter_nothing and left_key != right_key:
                self._raise_exception_if_nonsorted(right_key, right_group == iter_nothing)

            while left_group != iter_nothing and right_group != iter_nothing:
                if left_key < right_key:
                    yield from self.joiner(self.keys, left_group, iter_nothing)
                    left_key, left_group = next(left_groups, default_next)
                    self._raise_exception_if_nonsorted(left_key, left_group == iter_nothing, right_key)
                elif left_key > right_key:
                    yield from self.joiner(self.keys, iter_nothing, right_group)
                    right_key, right_group = next(right_groups, default_next)
                    self._raise_exception_if_nonsorted(right_key, right_group == iter_nothing, left_key)
                else:
                    yield from self.joiner(self.keys, left_group, right_group)
                    left_key, left_group = next(left_groups, default_next)
                    right_key, right_group = next(right_groups, default_next)

                    self._raise_exception_if_nonsorted(left_key, left_group == iter_nothing, right_key)
                    if right_group != iter_nothing and left_key != right_key:
                        self._raise_exception_if_nonsorted(right_key, right_group == iter_nothing, left_key)

            while left_group != iter_nothing:
                yield from self.joiner(self.keys, left_group, iter_nothing)
                left_key, left_group = next(left_groups, default_next)
                self._raise_exception_if_nonsorted(left_key, left_group == iter_nothing, right_key)

            while right_group != iter_nothing:
                yield from self.joiner(self.keys, iter_nothing, right_group)
                right_key, right_group = next(right_groups, default_next)
                self._raise_exception_if_nonsorted(right_key, right_group == iter_nothing, left_key)
        else:
            yield from self.joiner(self.keys, iter(rows), iter(args[0]))

        self._used_keys = list()


# Dummy operators


class DummyMapper(Mapper):
    """Yield exactly the row passed"""
    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        yield row


class FirstReducer(Reducer):
    """Yield only first row from passed ones"""
    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        for row in rows:
            yield row
            break


# Mappers


class FilterPunctuation(Mapper):
    """Left only non-punctuation symbols"""
    def __init__(self, column: str):
        """
        :param column: name of column to process
        """
        self.column = column

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        row[self.column] = ''.join([letter for letter in row[self.column] if letter not in string.punctuation])
        yield row


class LowerCase(Mapper):
    """Replace column value with value in lower case"""
    def __init__(self, column: str):
        """
        :param column: name of column to process
        """
        self.column = column

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        row[self.column] = row[self.column].lower()
        yield row


class Split(Mapper):
    """Split row on multiple rows by separator"""
    def __init__(self, column: str, separator: tp.Optional[str] = None) -> None:
        """
        :param column: name of column to split
        :param separator: string to separate by
        """
        self.column = column
        self.separator = separator

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        for splitted_substring in row[self.column].split(self.separator):
            new_row = deepcopy(row)
            new_row[self.column] = splitted_substring
            yield new_row


class Product(Mapper):
    """Calculates product of multiple columns"""
    def __init__(self, columns: tp.Sequence[str], result_column: str = 'product') -> None:
        """
        :param columns: column names to product
        :param result_column: column name to save product in
        """
        self.columns = columns
        self.result_column = result_column

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        row[self.result_column] = np.prod([row[col_to_prod] for col_to_prod in self.columns])
        yield row


class Filter(Mapper):
    """Remove records that don't satisfy some condition"""
    def __init__(self, condition: tp.Callable[[TRow], bool]) -> None:
        """
        :param condition: if condition is not true - remove record
        """
        self.condition = condition

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        if self.condition(row):
            yield row


class Project(Mapper):
    """Leave only mentioned columns"""
    def __init__(self, columns: tp.Sequence[str]) -> None:
        """
        :param columns: names of columns to leave
        """
        self.columns = columns

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        new_row = {col: row[col] for col in self.columns}
        yield new_row


class LogOfDivision(Mapper):
    """compute log(numerator / demoninator)"""
    def __init__(self, numerator: str, demoninator: str, result_column: str = 'division') -> None:
        """
        :param numerator: column with numerator
        :param demoninator: column with demoninator
        :param result_column: column name to save result in
        """
        self.numerator = numerator
        self.demoninator = demoninator
        self.result_column = result_column

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        row[self.result_column] = np.log(row[self.numerator]) - np.log(row[self.demoninator])
        yield row


class SplitWeekHoursToTime(Mapper):
    """compute log(numerator / demoninator)"""

    def __init__(self, column_start: str, column_end: str,
                 result_weekday_column: str = 'weekday', result_hour_column: str = 'hour',
                 result_time_in_seconds: str = 'seconds', result_path_share: str = 'path_share') -> None:
        """
        :param column_start: entrance time to a road.example: 20171020T090548.9
        :param column_end: exit time from the road. example: 20171020T090548.9
        :param result_weekday_column: extracted weekdays between entrance and exit times
        :param result_hour_column: extracted hours between entrance and exit times
        :param result_time_in_seconds: seconds on the road in a particular hour
        :param result_path_share: road share that traveled at a particular hour
        """
        self.column_start = column_start
        self.column_end = column_end
        self.row_cols = [result_weekday_column, result_hour_column,
                         result_time_in_seconds, result_path_share]
        self.WEEKDAYS = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")

    def get_time_row(self, weekday: str, hour: int, time_in_seconds: float, path_share: float) -> tp.Dict[str, tp.Any]:
        row: tp.Dict[str, tp.Any] = {}
        for key, value in zip(self.row_cols, [weekday, hour, time_in_seconds, path_share]):
            row[key] = value
        return row

    def str_date_to_date(self, str_date: str) -> datetime:
        pattern = '%Y%m%dT%H%M%S.%f' if '.' in str_date else '%Y%m%dT%H%M%S'
        return datetime.strptime(str_date, pattern)

    def get_first_hour_ending_row(self, date_start: datetime, all_path_in_seconds: float) -> tp.Dict[str, tp.Any]:
        first_hour_end = date_start.replace(minute=0, second=0, microsecond=0) + timedelta(hours=1)
        first_hour_in_seconds = (first_hour_end - date_start).total_seconds()

        new_row = self.get_time_row(self.WEEKDAYS[int(date_start.date().weekday())],
                                    date_start.hour,
                                    first_hour_in_seconds,
                                    first_hour_in_seconds / all_path_in_seconds)
        return new_row

    def get_last_hour_starting_row(self, date_end: datetime, all_path_in_seconds: float) -> tp.Dict[str, tp.Any]:
        last_hour_start = date_end.replace(minute=0, second=0, microsecond=0)
        last_hour_in_seconds = (date_end - last_hour_start).total_seconds()

        new_row = self.get_time_row(self.WEEKDAYS[int(date_end.date().weekday())],
                                    date_end.hour,
                                    last_hour_in_seconds,
                                    last_hour_in_seconds / all_path_in_seconds)
        return new_row

    def get_period_during_one_day_row(self, date_start: datetime, date_end: datetime) -> tp.Dict[str, tp.Any]:
        all_path_in_seconds = (date_end - date_start).total_seconds()
        new_row = self.get_time_row(self.WEEKDAYS[int(date_start.date().weekday())],
                                    date_start.hour,
                                    all_path_in_seconds,
                                    1.0
                                    )
        return new_row

    def get_full_hour_shifted_from_date_row(self, date_start: datetime, hour_id: int,
                                            all_path_in_seconds: float) -> tp.Dict[str, tp.Any]:
        weekday = self.WEEKDAYS[int((date_start + timedelta(hours=hour_id)).date().weekday())]

        new_row = self.get_time_row(weekday,
                                    (date_start + timedelta(hours=hour_id)).hour,
                                    60 * 60,
                                    60 * 60 / all_path_in_seconds
                                    )
        return new_row

    def count_total_hours_between_dates(self, date_start: datetime, date_end: datetime) -> int:
        first_hour_end = date_start.replace(minute=0, second=0, microsecond=0) + timedelta(hours=1)
        last_hour_start = date_end.replace(minute=0, second=0, microsecond=0)

        first_hour_in_seconds = (first_hour_end - date_start).total_seconds()
        last_hour_in_seconds = (date_end - last_hour_start).total_seconds()
        all_path_in_seconds = (date_end - date_start).total_seconds()

        cnt_full_hours_between = int((all_path_in_seconds - first_hour_in_seconds - last_hour_in_seconds) / 60 / 60)
        return cnt_full_hours_between

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        date_start = self.str_date_to_date(row[self.column_start])
        date_end = self.str_date_to_date(row[self.column_end])

        all_path_in_seconds = (date_end - date_start).total_seconds()
        if int(all_path_in_seconds) == 0:
            return None

        if date_start.date() == date_end.date() and date_start.hour == date_end.hour:
            new_row = self.get_period_during_one_day_row(date_start, date_end)
            row.update(new_row)
            yield row
        else:
            cnt_full_hours_between = self.count_total_hours_between_dates(date_start, date_end)
            boundary_intervals = [self.get_first_hour_ending_row(date_start, all_path_in_seconds),
                                  self.get_last_hour_starting_row(date_end, all_path_in_seconds),
                                  ]
            for travel_interval_row in boundary_intervals:
                row.update(travel_interval_row)
                yield row

            for hour_id in range(1, cnt_full_hours_between + 1):
                hour_row = self.get_full_hour_shifted_from_date_row(date_start, hour_id, all_path_in_seconds)
                row.update(hour_row)
                yield row


class CalculateGPSLength(Mapper):
    """Calculate the great circle distance in kilometers between two points
       on the earth (specified in decimal degrees)
       """
    def __init__(self, column_start: str, column_end: str, result_column: str = 'length') -> None:
        """
        :param column_start: the first point of an edge, pair (longitude, latitude)
        :param column_end: the second point of an edge, pair (longitude, latitude)
        :param result_column: column to save resulted distance in
        """
        self.column_start = column_start
        self.column_end = column_end
        self.result_column = result_column

    def haversine_formula(self, lon1: float, lat1: float, lon2: float, lat2: float) -> float:
        """
        copied from https://stackoverflow.com/questions/4913349/haversine-formula-in-python ...
        ... -bearing-and-distance-between-two-gps-points/4913653#4913653
        :param lon1: longitude of the first point
        :param lat1: latitude of the first point
        :param lon2: longitude of the second point
        :param lat2: latitude of the second point
        """
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        r = 6371  # Radius of earth in kilometers. Use 3956 for miles
        return c * r

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        lon1, lat1 = row[self.column_start]
        lon2, lat2 = row[self.column_end]
        row[self.result_column] = self.haversine_formula(lon1, lat1, lon2, lat2)
        yield row


class CalculateSpeedInKmPerHour(Mapper):
    """Calculate speed in km per hours from distance and time"""
    def __init__(self, column_path_length: str, column_time_in_seconds: str,
                 column_path_share: str, result_column: str = 'speed') -> None:
        """
        :param column_path_length: column with total length of used road
        :param column_time_in_seconds: column with time of travelling
        through a road, in seconds
        :param column_path_share: column with total share of used road
        :param result_column: column to save resulted speed in
        """
        self.column_path_length = column_path_length
        self.column_time_in_seconds = column_time_in_seconds
        self.column_path_share = column_path_share
        self.result_column = result_column

    def __call__(self, row: TRow) -> TRowsGenerator:
        """
        :param row: table row
        """
        length_in_km = row[self.column_path_length] * row[self.column_path_share]
        time_in_hours = row[self.column_time_in_seconds] / 60 / 60
        row[self.result_column] = length_in_km / time_in_hours
        yield row


# Reducers


class TopN(Reducer):
    """Calculate top N by value"""
    def __init__(self, column: str, n: int) -> None:
        """
        :param column: column name to get top by
        :param n: number of top values to extract
        """
        self.column_max = column
        self.n = n

    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        for new_row in heapq.nlargest(self.n, rows, key=lambda row: row[self.column_max]):
            yield new_row


def get_first_row_values_by_keys(rows: tp.Iterable[tp.Dict[str, tp.Any]],
                                 keys: tp.List[str]) -> tp.Dict[str, tp.Any]:
    new_row: tp.Dict[str, tp.Any] = {}
    for row in rows:
        new_row = {key: row[key] for key in keys}
        break
    return new_row


class TermFrequency(Reducer):
    """Calculate frequency of values in column"""
    def __init__(self, words_column: str, result_column: str = 'tf') -> None:
        """
        :param words_column: name for column with words
        :param result_column: name for result column
        """
        self.words_column = words_column
        self.result_column = result_column

    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        counts = Counter([row[self.words_column] for row in rows])
        cnt_terms = sum(counts.values())
        for term, cnt in counts.items():
            new_row = get_first_row_values_by_keys(rows, group_key)
            new_row[self.words_column] = term
            new_row[self.result_column] = cnt / cnt_terms
            yield new_row


class Count(Reducer):
    """Count rows passed and yield single row as a result"""
    def __init__(self, column: str) -> None:
        """
        :param column: name of column to count
        """
        self.column = column

    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        new_row = get_first_row_values_by_keys(rows, group_key)
        new_row[self.column] = len(list(rows))
        yield new_row


class Sum(Reducer):
    """Sum values in column passed and yield single row as a result"""
    def __init__(self, columns: tp.Union[tp.List[str], str]) -> None:
        """
        :param column: name of column to sum
        """
        self.columns: tp.List[str] = [columns] if isinstance(columns, str) else columns

    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        new_row = get_first_row_values_by_keys(rows, group_key)
        for column_to_sum in self.columns:
            new_row[column_to_sum] = sum([row[column_to_sum] for row in rows])
        yield new_row


class Mean(Reducer):
    """Mean values in column passed and yield single row as a result"""
    def __init__(self, column: str) -> None:
        """
        :param column: name of column to mean
        """
        self.column = column

    def __call__(self, group_key: tp.List[str], rows: TRowsIterable) -> TRowsGenerator:
        """
        :param group_key: list of keys of reduce
        :param rows: table rows
        """
        new_row = get_first_row_values_by_keys(rows, group_key)
        new_row[self.column] = np.mean([row[self.column] for row in rows])
        yield new_row


# Joiners


def JoinRows(keys: tp.Sequence[str], a_row: TRow, b_row: TRow, suffix_a: str, suffix_b: str) -> TRow:
    """
    :param keys: list of join keys
    :param a_row: row of the first table
    :param b_row: row of the second table
    :param suffix_a: will be append as a suffix to the key names
    in the first table in case of the keys coincidence in tables
    :param suffix_b: will be append as a suffix to the key names
    in the second table in case of the keys coincidence in tables
    """
    joined_row = {key: a_row[key] for key in keys}
    for key_to_suffix in [key for key in a_row if key in b_row and key not in keys]:
        joined_row[key_to_suffix + suffix_a] = a_row[key_to_suffix]
        joined_row[key_to_suffix + suffix_b] = b_row[key_to_suffix]
    for key in [key for key in a_row if key not in b_row]:
        joined_row[key] = a_row[key]
    for key in [key for key in b_row if key not in a_row]:
        joined_row[key] = b_row[key]
    return joined_row


class InnerJoiner(Joiner):
    """Join with inner strategy"""
    def __call__(self, keys: tp.Sequence[str], rows_a: TRowsIterable, rows_b: TRowsIterable) -> TRowsGenerator:
        """
        :param keys: list of join keys
        :param rows_a: rows of the first table
        :param rows_b: rows of the second table
        """
        rows_a = list(rows_a)
        rows_b = list(rows_b)

        if not rows_a or not rows_b:
            return

        for row_a in rows_a:
            for row_b in rows_b:
                yield JoinRows(keys, row_a, row_b, self._a_suffix, self._b_suffix)


class OuterJoiner(Joiner):
    """Join with outer strategy"""
    def __call__(self, keys: tp.Sequence[str], rows_a: TRowsIterable, rows_b: TRowsIterable) -> TRowsGenerator:
        """
        :param keys: list of join keys
        :param rows_a: rows of the first table
        :param rows_b: rows of the second table
        """
        rows_a = list(rows_a)
        rows_b = list(rows_b)

        for row_a in rows_a:
            for row_b in rows_b:
                yield JoinRows(keys, row_a, row_b, self._a_suffix, self._b_suffix)
            if not rows_b:
                yield row_a

        if not rows_a:
            yield from rows_b


class LeftJoiner(Joiner):
    """Join with left strategy"""
    def __call__(self, keys: tp.Sequence[str], rows_a: TRowsIterable, rows_b: TRowsIterable) -> TRowsGenerator:
        """
        :param keys: list of join keys
        :param rows_a: rows of the first table
        :param rows_b: rows of the second table
        """
        rows_a = list(rows_a)
        rows_b = list(rows_b)

        for row_b in rows_b:
            for row_a in rows_a:
                yield JoinRows(keys, row_a, row_b, self._a_suffix, self._b_suffix)
        if not rows_b:
            yield from rows_a


class RightJoiner(Joiner):
    """Join with right strategy"""
    def __call__(self, keys: tp.Sequence[str], rows_a: TRowsIterable, rows_b: TRowsIterable) -> TRowsGenerator:
        """
        :param keys: list of join keys
        :param rows_a: rows of the first table
        :param rows_b: rows of the second table
        """
        rows_a = list(rows_a)
        rows_b = list(rows_b)

        for row_a in rows_a:
            for row_b in rows_b:
                yield JoinRows(keys, row_a, row_b, self._a_suffix, self._b_suffix)
        if not rows_a:
            yield from rows_b

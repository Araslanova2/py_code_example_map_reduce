import typing as tp

from copy import deepcopy
from typing import NamedTuple

from .external_sort import ExternalSort
from . import operations as ops


class OperationTuple(NamedTuple):
    """NamedTuple with operations to be run"""
    action: tp.Any
    table_to_join: tp.Optional[tp.Any] = None


class Graph:
    """Computational graph implementation"""
    def __init__(self) -> None:
        self.input_stream_name = ""
        self._operations_to_run: tp.List[tp.Any] = []

    @staticmethod
    def graph_from_iter(input_stream_name: str) -> 'Graph':
        """Construct new graph which reads data from row iterator (in form of sequence of Rows
        from 'kwargs' passed to 'run' method) into graph data-flow
        :param name: name of kwarg to use as data source or path to txt file.
        If txt file were passes, run method shoud get `parser` function to parse lines of the txt file
        """
        graph = Graph()
        graph.input_stream_name = input_stream_name
        return graph

    @staticmethod
    def graph_from_file(filename: str, parser: tp.Callable[[str], ops.TRow]) -> 'Graph':
        """Construct new graph extended with operation for reading rows from file
        :param filename: filename to read from
        :param parser: parser from string to Row
        """
        def _iterate_file(filename: str, parser: tp.Callable[[str], ops.TRow]) -> ops.TRowsGenerator:
            """Yielding rows from file
            :param filename: filename to read from
            :param parser: parser from string to Row
            """
            file = open(filename, 'r')
            for line in file:
                yield parser(line)
            file.close()

        def read_file(no_obj: tp.Any, no_join: tp.Any) -> ops.TRowsGenerator:
            return _iterate_file(filename, parser)

        output_graph = Graph()
        output_graph._operations_to_run.append(OperationTuple(read_file))
        return output_graph

    def map(self, mapper: ops.Mapper) -> 'Graph':
        """Construct new graph extended with map operation with particular mapper
        :param mapper: mapper to use
        """
        new_graph = deepcopy(self)
        new_graph._operations_to_run.append(OperationTuple(ops.Map(mapper)))
        return new_graph

    def reduce(self, reducer: ops.Reducer, keys: tp.List[str]) -> 'Graph':
        """Construct new graph extended with reduce operation with particular reducer
        :param reducer: reducer to use
        :param keys: keys for grouping
        """
        new_graph = deepcopy(self)
        new_graph._operations_to_run.append(OperationTuple(ops.Reduce(reducer, keys)))
        return new_graph

    def sort(self, keys: tp.Sequence[str]) -> 'Graph':
        """Construct new graph extended with sort operation
        :param keys: sorting keys (typical is tuple of strings)
        """
        new_graph = deepcopy(self)
        new_graph._operations_to_run.append(OperationTuple(ExternalSort(keys)))
        return new_graph

    def join(self, joiner: ops.Joiner, join_graph: 'Graph', keys: tp.Sequence[str]) -> 'Graph':
        """Construct new graph extended with join operation with another graph
        :param joiner: join strategy to use
        :param join_graph: other graph to join with
        :param keys: keys for grouping
        """
        new_graph = deepcopy(self)
        new_graph._operations_to_run.append(OperationTuple(ops.Join(joiner, keys), table_to_join=join_graph))
        return new_graph

    def run(self, **kwargs: tp.Any) -> tp.List[ops.TRow]:
        """Single method to start execution; data sources passed as kwargs
        :param kwargs: data iterator builder should be passed as named arguments:
        names of arguments shoul coinside with graph_from_iter source;
        If txt file name were passed, run method
        shoud get `parser` function to parse lines of the txt file
        """
        return list(self._run_return_generator(**kwargs))

    def _run_return_generator(self, **kwargs: tp.Any) -> ops.TRowsGenerator:
        """Same as run, but returns an intermediate generator:
        Next `run` will convert the iterator to final table
        :param kwargs: data iterators builder should be passed as named arguments:
        names of arguments shoul coinside with graph_from_iter source;
        If txt file name were passed, run method
        shoud get `parser` function to parse lines of the txt file
        """
        obj_to_process = ops.default_generator()
        if self.input_stream_name:
            iter_builder = kwargs[self.input_stream_name]
            obj_to_process = iter_builder()

        for operation in self._operations_to_run:
            action = operation.action
            table_to_join_if_any = operation[1]._run_return_generator(**kwargs) if operation.table_to_join is not None \
                else None
            obj_to_process = action(obj_to_process, table_to_join_if_any)
        return obj_to_process

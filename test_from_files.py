import os
import matplotlib.pyplot as plt
import typing as tp

from operator import itemgetter
from pytest import approx

from . import graphs_from_files as graphs

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

# works 85s on my computer
def test_word_count_from_file() -> None:
    graph = graphs.word_count_graph(SCRIPT_DIR + '/resource/text_corpus.txt',
                                    text_column='text', count_column='count')
    result = graph.run(parser=lambda line: eval(line))

    expected_head5 = [
        {'text': '00', 'count': 1},
        {'text': '01', 'count': 1},
        {'text': '02', 'count': 1},
        {'text': '03', 'count': 1},
        {'text': '04', 'count': 1}
    ]
    expected_tail5 = [
        {'text': 'a', 'count': 52345},
        {'text': 'to', 'count': 65782},
        {'text': 'of', 'count': 72998},
        {'text': 'and', 'count': 87196},
        {'text': 'the', 'count': 151741}
    ]

    assert result[:5] == expected_head5
    assert result[-5:] == expected_tail5

# works 200s on my computer
def test_tf_idf_from_file() -> None:
    graph = graphs.inverted_index_graph(SCRIPT_DIR + '/resource/text_corpus.txt',
                                        doc_column='doc_id', text_column='text', result_column='tf_idf')
    result = graph.run(parser=lambda line: eval(line))

    result_sorted = sorted(result, key=itemgetter('tf_idf'))

    expected_tail5 = [
        {'doc_id': 'metamorphosis', 'text': 'gregor', 'tf_idf': approx(0.023353661094225653, 0.001)},
        {'doc_id': 'alice_adventures_in_wonderland', 'text': 'alice', 'tf_idf': approx(0.024179764404282315, 0.001)},
        {'doc_id': 'siddhartha', 'text': 'siddhartha', 'tf_idf': approx(0.026471732923049645, 0.001)},
        {'doc_id': 'the_importance_of_being_earnest', 'text': 'cecily', 'tf_idf': approx(0.03167404288955937, 0.001)},
        {'doc_id': 'the_importance_of_being_earnest', 'text': 'algernon', 'tf_idf': approx(0.033419856277172, 0.001)},
    ]

    assert result_sorted[-5:] == expected_tail5

# works 200s on my computer
def test_pmi_from_file() -> None:
    graph = graphs.pmi_graph(SCRIPT_DIR + '/resource/text_corpus.txt',
                             doc_column='doc_id', text_column='text', result_column='pmi')
    result = graph.run(parser=lambda line: eval(line))

    result_first = {'doc_id': 'a_tale_of_two_cities', 'text': 'abbaye', 'pmi': approx(2.937198471074348, 0.001)}
    result_last = {'doc_id': 'war_and_peace', 'text': 'adroitness', 'pmi': approx(1.390442066308827, 0.001)}

    assert result[0] == result_first
    assert result[-1] == result_last

def plot_yandex_map_avg_speed(result: tp.List[tp.Dict[str, tp.Any]]) -> None:
    WEEKDAYS = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")

    weekdays_to_speed: tp.Dict[str, tp.Dict[str, tp.Any]] = {weekday : {'hour' : [],
                                                                        'speed' : []} for weekday in WEEKDAYS}
    for line in result:
        weekdays_to_speed[line['weekday']]['hour'].append(line['hour'])
        weekdays_to_speed[line['weekday']]['speed'].append(line['speed'])

    fig = plt.figure(figsize=(30, 10))
    for weekday in weekdays_to_speed:
        hours, speed = zip(*sorted(zip(weekdays_to_speed[weekday]['hour'],
                                       weekdays_to_speed[weekday]['speed'])))
        plt.plot(hours, speed, label=weekday)
    plt.xticks(list(range(24)))
    plt.xlabel('Hour', fontsize=16)
    plt.ylabel('Avg Speed', fontsize=16)
    plt.title('Average speed per hour over weekdays', fontsize=24)
    plt.legend(fontsize=16)
    plt.savefig('yandex_map_avg_speed.png', bbox_inches='tight')

# works 80s  on my computer
def test_yandex_maps() -> None:
    graph = graphs.yandex_maps_graph(
        SCRIPT_DIR + '/resource/travel_times.txt', SCRIPT_DIR + '/resource/road_graph_data.txt',
        enter_time_column='enter_time', leave_time_column='leave_time', edge_id_column='edge_id',
        start_coord_column='start', end_coord_column='end',
        weekday_result_column='weekday', hour_result_column='hour', speed_result_column='speed'
    )

    result = graph.run(parser=lambda line: eval(line))
    plot_yandex_map_avg_speed(result)

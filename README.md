### Map-Reduce

* The structure of the project

./lib/graph.py - an implementation of computional graph with operations: map, reduce, join, sort
./lib/operations.py - operations to substitute into the graph's: map, reduce, join у графа
./lib/external_sort.py - sorting dunction to imitate external sort (copied from somewhere)
./lib/memory_watchdog.py - calculation of peak memory (for tests)

./resource - a folder for external resourses, i.e. ./resource/text_corpus.txt - created after unpacking the archive with tests

* About tests:
In the root directory you can find several files with tests:

test_light.py - light tests for correctness
test_operations.py - tests for ./lib/operations.py

test_graph.py - tests for ./lib/graph.py

test_from_files.py - tests that require files from the ./resource folder
test_graph_from_file.py - graphs, which use a function that reads from files. tests similar to the test_graph.py


import os

from operator import itemgetter
from pytest import approx

from .lib import operations as ops
from .lib.graph import Graph


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))


def test_graph_from_iter_from_iter() -> None:
    tests: ops.TRowsIterable = [
        {'test_id': 1, 'text': 'one two three'},
        {'test_id': 2, 'text': 'tab\tsplitting\ttest'},
        {'test_id': 3, 'text': 'more\nlines\ntest'},
        {'test_id': 4, 'text': 'tricky\u00A0test'}
    ]

    graph = Graph().graph_from_iter('tests')
    result = graph.run(tests=lambda: iter(tests))
    assert tests == result


# # Put file ./resource/road_graph_data.txt in resource directory and uncomment this test
# def test_graph_from_iter_from_file() -> None:

#     etalon_first_road_points_data_from_file = {"start": [37.84870228730142, 55.73853974696249],
#                                                "end": [37.8490418381989, 55.73832445777953],
#                                                "edge_id": 8414926848168493057}
#     etalon_last_road_points_data_from_file = {"start": [37.52068396657705, 55.733815035782754],
#                                               "end": [37.52044826745987, 55.734108528122306],
#                                               "edge_id": 3233503035421523970}

#     graph = Graph().graph_from_file(SCRIPT_DIR + '/resource/road_graph_data.txt',
#                                     parser=lambda line: eval(line))
#     result = graph.run()
#     assert result[0] == etalon_first_road_points_data_from_file
#     assert result[-1] == etalon_last_road_points_data_from_file


def test_map_by_splitting() -> None:
    tests: ops.TRowsIterable = [
        {'test_id': 1, 'text': 'one two three'},
        {'test_id': 2, 'text': 'tab\tsplitting\ttest'},
        {'test_id': 3, 'text': 'more\nlines\ntest'},
        {'test_id': 4, 'text': 'tricky\u00A0test'}
    ]

    etalon: ops.TRowsIterable = [
        {'test_id': 1, 'text': 'one'},
        {'test_id': 1, 'text': 'three'},
        {'test_id': 1, 'text': 'two'},

        {'test_id': 2, 'text': 'splitting'},
        {'test_id': 2, 'text': 'tab'},
        {'test_id': 2, 'text': 'test'},

        {'test_id': 3, 'text': 'lines'},
        {'test_id': 3, 'text': 'more'},
        {'test_id': 3, 'text': 'test'},

        {'test_id': 4, 'text': 'test'},
        {'test_id': 4, 'text': 'tricky'}
    ]

    graph = Graph().graph_from_iter('tests').map(ops.Split(column='text'))
    result = graph.run(tests=lambda: iter(tests))

    assert etalon == sorted(result, key=itemgetter('test_id', 'text'))


def test_reduce_by_term_frequency() -> None:
    docs: ops.TRowsIterable = [
        {'doc_id': 1, 'text': 'hello', 'count': 1},
        {'doc_id': 1, 'text': 'little', 'count': 1},
        {'doc_id': 1, 'text': 'world', 'count': 1},

        {'doc_id': 2, 'text': 'little', 'count': 1},

        {'doc_id': 3, 'text': 'little', 'count': 3},
        {'doc_id': 3, 'text': 'little', 'count': 3},
        {'doc_id': 3, 'text': 'little', 'count': 3},

        {'doc_id': 4, 'text': 'little', 'count': 2},
        {'doc_id': 4, 'text': 'hello', 'count': 1},
        {'doc_id': 4, 'text': 'little', 'count': 2},
        {'doc_id': 4, 'text': 'world', 'count': 1},

        {'doc_id': 5, 'text': 'hello', 'count': 2},
        {'doc_id': 5, 'text': 'hello', 'count': 2},
        {'doc_id': 5, 'text': 'world', 'count': 1},

        {'doc_id': 6, 'text': 'world', 'count': 4},
        {'doc_id': 6, 'text': 'world', 'count': 4},
        {'doc_id': 6, 'text': 'world', 'count': 4},
        {'doc_id': 6, 'text': 'world', 'count': 4},
        {'doc_id': 6, 'text': 'hello', 'count': 1}
    ]
    presorted_docs = sorted(docs, key=itemgetter('doc_id'))

    etalon: ops.TRowsIterable = [
        {'doc_id': 1, 'text': 'hello', 'tf': approx(0.3333, abs=0.001)},
        {'doc_id': 1, 'text': 'little', 'tf': approx(0.3333, abs=0.001)},
        {'doc_id': 1, 'text': 'world', 'tf': approx(0.3333, abs=0.001)},

        {'doc_id': 2, 'text': 'little', 'tf': approx(1.0)},

        {'doc_id': 3, 'text': 'little', 'tf': approx(1.0)},

        {'doc_id': 4, 'text': 'hello', 'tf': approx(0.25)},
        {'doc_id': 4, 'text': 'little', 'tf': approx(0.5)},
        {'doc_id': 4, 'text': 'world', 'tf': approx(0.25)},

        {'doc_id': 5, 'text': 'hello', 'tf': approx(0.666, abs=0.001)},
        {'doc_id': 5, 'text': 'world', 'tf': approx(0.333, abs=0.001)},

        {'doc_id': 6, 'text': 'hello', 'tf': approx(0.2)},
        {'doc_id': 6, 'text': 'world', 'tf': approx(0.8)}
    ]

    graph = Graph().graph_from_iter('presorted_docs').reduce(ops.TermFrequency(words_column='text',
                                                                               result_column='tf'), keys=['doc_id'])
    result = graph.run(presorted_docs=lambda: iter(presorted_docs))

    assert etalon == sorted(result, key=itemgetter('doc_id', 'text'))


def test_sort() -> None:
    tests: ops.TRowsIterable = [
        {'test_id': 3, 'text': 'more\nlines\ntest'},
        {'test_id': 2, 'text': 'tab\tsplitting\ttest'},
        {'test_id': 1, 'text': 'one two three'},
        {'test_id': 4, 'text': 'tricky\u00A0test'}
    ]

    etalon: ops.TRowsIterable = [
        {'test_id': 1, 'text': 'one two three'},
        {'test_id': 2, 'text': 'tab\tsplitting\ttest'},
        {'test_id': 3, 'text': 'more\nlines\ntest'},
        {'test_id': 4, 'text': 'tricky\u00A0test'}
    ]

    graph = Graph().graph_from_iter('tests').sort(['test_id'])
    result = graph.run(tests=lambda: iter(tests))

    assert etalon == result


def test_join() -> None:
    players: ops.TRowsIterable = [
        {'player_id': 0, 'username': 'root'},
        {'player_id': 1, 'username': 'XeroX'},
        {'player_id': 2, 'username': 'jay'}
    ]
    presorted_players = sorted(players, key=itemgetter('player_id'))

    games: ops.TRowsIterable = [
        {'game_id': 1, 'player_id': 3, 'score': 9999999},
        {'game_id': 2, 'player_id': 1, 'score': 17},
        {'game_id': 3, 'player_id': 2, 'score': 22}
    ]
    presorted_games = sorted(games, key=itemgetter('player_id'))

    etalon: ops.TRowsIterable = [
        {'game_id': 2, 'player_id': 1, 'score': 17, 'username': 'XeroX'},
        {'game_id': 3, 'player_id': 2, 'score': 22, 'username': 'jay'}
    ]

    graph_players = Graph().graph_from_iter('presorted_players')
    graph_games = Graph().graph_from_iter('presorted_games')
    graph_join_by_player = graph_games.join(ops.InnerJoiner(), join_graph=graph_players, keys=['player_id'])
    result = graph_join_by_player.run(presorted_players=lambda: iter(presorted_players),
                                      presorted_games=lambda: iter(presorted_games))

    assert etalon == result

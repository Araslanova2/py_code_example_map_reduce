from .lib.graph import Graph
from .lib import operations


def word_count_graph(filename: str, text_column: str = 'text', count_column: str = 'count') -> Graph:
    """Constructs graph which counts words in text_column of all rows passed
    :param filename: data name; an iterator with the same name should be passed in `run` call
    :param text_column: a column with text
    :param count_column: a column name to save number of words
    """
    return Graph.graph_from_file(filename, parser=lambda line: eval(line)) \
        .map(operations.FilterPunctuation(text_column)) \
        .map(operations.LowerCase(text_column)) \
        .map(operations.Split(text_column)) \
        .sort([text_column]) \
        .reduce(operations.Count(count_column), [text_column]) \
        .sort([count_column, text_column])


def inverted_index_graph(filename: str, doc_column: str = 'doc_id', text_column: str = 'text',
                         result_column: str = 'tf_idf') -> Graph:
    """Constructs graph which calculates td-idf for every word/document pair
    :param filename: data name; an iterator with the same name should be passed in `run` call
    :param doc_column: a column with document identifiers
    :param text_column: a column with text
    :param result_column: a column name to save words' tf-idf scores in
    """
    graph_split_word = Graph.graph_from_file(filename, parser=lambda line: eval(line)) \
        .map(operations.FilterPunctuation(text_column)) \
        .map(operations.LowerCase(text_column)) \
        .map(operations.Split(text_column))

    graph_count_docs = Graph.graph_from_file(filename, parser=lambda line: eval(line)) \
        .sort([doc_column]) \
        .reduce(operations.Count(column='all_docs_count'), keys=[])

    graph_idf = graph_split_word.sort([doc_column, text_column]) \
        .reduce(operations.FirstReducer(), keys=[doc_column, text_column]) \
        .sort([text_column]) \
        .reduce(operations.Count(column='word_count'), keys=[text_column]) \
        .join(operations.InnerJoiner(), join_graph=graph_count_docs, keys=[]) \
        .map(operations.LogOfDivision(numerator='all_docs_count', demoninator='word_count', result_column='idf'))

    graph_tf = graph_split_word.sort([doc_column]) \
        .reduce(operations.TermFrequency(words_column=text_column, result_column='tf'), keys=[doc_column])

    graph_tf_idf = graph_tf.sort([text_column]) \
        .join(operations.InnerJoiner(), join_graph=graph_idf, keys=[text_column]) \
        .map(operations.Product(['tf', 'idf'], result_column=result_column)) \
        .sort([text_column, result_column]) \
        .reduce(operations.TopN(result_column, 3), keys=[text_column]) \
        .map(operations.Project([doc_column, text_column, result_column]))
    return graph_tf_idf


def pmi_graph(filename: str, doc_column: str = 'doc_id', text_column: str = 'text',
              result_column: str = 'pmi') -> Graph:
    """Constructs graph which gives for every document the top 10 words ranked by pointwise mutual information
    :param filename: data name; an iterator with the same name should be passed in `run` call
    :param doc_column: a column with document identifiers
    :param text_column: a column with text
    :param result_column: a column name to save words' pmi scores in
    """
    graph_split_word = Graph.graph_from_file(filename, parser=lambda line: eval(line)) \
        .map(operations.FilterPunctuation(text_column)) \
        .map(operations.LowerCase(text_column)) \
        .map(operations.Split(text_column)) \
        .map(operations.Filter(condition=lambda row: len(row[text_column]) > 4)) \
        .sort([doc_column, text_column]) \

    graph_appropriate_word = graph_split_word.reduce(operations.Count('count_doc_word'), [doc_column, text_column]) \
        .map(operations.Filter(condition=lambda row: row['count_doc_word'] >= 2))

    graph_split_filter_word = graph_split_word.join(operations.InnerJoiner(), join_graph=graph_appropriate_word,
                                                    keys=[doc_column, text_column]) \

    graph_tf_by_doc = graph_split_filter_word.sort([doc_column]) \
        .reduce(operations.TermFrequency(words_column=text_column, result_column='tf_by_doc'), keys=[doc_column]) \
        .sort([text_column]) \

    graph_tf_over_all = graph_split_filter_word \
        .reduce(operations.TermFrequency(words_column=text_column, result_column='tf_over_all'), keys=[]) \
        .sort([text_column]) \

    graph_pmi = graph_tf_by_doc.join(operations.InnerJoiner(), join_graph=graph_tf_over_all, keys=[text_column]) \
        .map(operations.LogOfDivision(numerator='tf_by_doc', demoninator='tf_over_all', result_column='pmi'))  \
        .sort([doc_column, text_column]) \
        .reduce(operations.TopN(result_column, 10), [doc_column]) \
        .map(operations.Project([doc_column, text_column, 'pmi']))

    return graph_pmi


def yandex_maps_graph(filename_time: str, filename_length: str,
                      enter_time_column: str = 'enter_time', leave_time_column: str = 'leave_time',
                      edge_id_column: str = 'edge_id', start_coord_column: str = 'start',
                      end_coord_column: str = 'end', weekday_result_column: str = 'weekday',
                      hour_result_column: str = 'hour', speed_result_column: str = 'speed') -> Graph:
    """Constructs graph which measures average speed in km/h depending on the weekday and hour
    :param filename_time: time data name; an iterator with the same name should be passed in `run` call
    :param filename_length: roads coordiantes data name
    :param enter_time_column: column with entrance times to roads
    :param leave_time_column: column with exit times from roads
    :param edge_id_column: column with road ids
    :param start_coord_column: column with the first points of an roads, pairs (longitude, latitude)
    :param end_coord_column: column with the second points of an roads, pairs (longitude, latitude)
    :param weekday_result_column: resulted column with extracted weekdays between entrance and exit times
    :param hour_result_column: resulted column with extracted hours between entrance and exit times
    :param speed_result_column: resulted column with mean speed on a certain road in a certain weekday-hour in km/h
    """
    graph_week_hours_to_time = Graph.graph_from_file(filename_time, parser=lambda line: eval(line)) \
        .map(operations.SplitWeekHoursToTime(column_start=enter_time_column, column_end=leave_time_column,
                                             result_weekday_column=weekday_result_column, result_hour_column='hour',
                                             result_time_in_seconds='seconds', result_path_share='path_share')) \
        .sort([weekday_result_column, 'hour']) \
        .reduce(operations.Sum(columns=['seconds', 'path_share']), [weekday_result_column, 'hour', edge_id_column]) \
        .sort([edge_id_column])

    graph_lengths = Graph.graph_from_file(filename_length, parser=lambda line: eval(line)) \
        .map(operations.CalculateGPSLength(column_start=start_coord_column,
                                           column_end=end_coord_column,
                                           result_column='length')) \
        .map(operations.Project([edge_id_column, 'length'])) \
        .sort([edge_id_column])

    graph_speed = graph_week_hours_to_time.join(operations.InnerJoiner(),
                                                join_graph=graph_lengths,
                                                keys=[edge_id_column]) \
        .map(operations.CalculateSpeedInKmPerHour(column_path_length='length',
                                                  column_time_in_seconds='seconds',
                                                  column_path_share='path_share',
                                                  result_column=speed_result_column)) \
        .sort([weekday_result_column, 'hour']) \
        .reduce(operations.Mean(column=speed_result_column), [weekday_result_column, 'hour'])

    return graph_speed
